ruby-ftw (0.0.44-3) UNRELEASED; urgency=medium

  * Set upstream metadata fields: Repository-Browse.
  * Update standards version to 4.6.1, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Thu, 01 Dec 2022 07:43:06 -0000

ruby-ftw (0.0.44-2) unstable; urgency=medium

  * Team upload.

  [ Cédric Boutillier ]
  * Bump debhelper compatibility level to 9
  * Remove version in the gem2deb build-dependency
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Run wrap-and-sort on packaging files

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Use secure URI in debian/watch.
  * Use secure URI in Homepage field.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.
  * Update watch file format version to 4.
  * Bump debhelper from old 12 to 13.

  [ Lucas Kanashiro ]
  * Runtime depends on ${ruby:Depends} instead of the ruby interpreter
  * Declare compliance with Debian Policy 4.6.0

 -- Lucas Kanashiro <kanashiro@debian.org>  Wed, 20 Oct 2021 17:42:03 -0300

ruby-ftw (0.0.44-1) unstable; urgency=medium

  * Initial release (Closes: #757074)

 -- Tim Potter <tpot@hp.com>  Tue, 21 Jul 2015 11:06:14 +1100
